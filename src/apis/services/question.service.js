const ApiError = require('../../utils/api-error')
const httpStatus = require('http-status')
const { Questions, QuestionItems, Exams } = require('../models')

const getAll = async () => {
    return await Questions.find({ isDeleted: false }).populate('choices')
}

const getById = async ({ _id }) => {
    return await Questions.find({ _id, isDeleted: false }).populate('choices')
}

const create = async ({ choices, answer,examId, ...rest }) => {
    choices = await Promise.all(
        choices.map(async (item) => {
            const questionItem = await QuestionItems.create({ label: item?.label })
            if (item?._id === answer?._id) {
                answer = {
                    _id: questionItem._id,
                }
            }

            return {
                _id: questionItem._id,
            }
        })
    )
    return await Questions.create({
        choices,
        answer,
        examId,
        ...rest,
    })
}

const update = async ({ _id, choices, ...rest }) => {
    choices = await Promise.all(
        choices.map(async (item) => {
            await QuestionItems.findOneAndUpdate({ _id: item._id }, { label: item?.label })
        })
    )
    return await Questions.findOneAndUpdate({ _id, isDeleted: false }, { ...rest })
}

const deleteQuestion = async ({ _id }) => {
    return await Questions.findOneAndUpdate({ _id }, { isDeleted: true })
}

const addQuestionToExam = async ({ choices, answer, examId,...rest }) => {

    let totalTopic = await Questions.find({ examId: examId, isDeleted: false })

    let TA = totalTopic.filter(item => item.section.toString() === '634e752d943636043eb1b9ea').length
    let VH = totalTopic.filter(item => item.section.toString() === '634e6ef3943636043eb1b95b').length
    let TH = totalTopic.filter(item => item.lesson.toString() === '634e79ba943636043eb1ba54').length
    let TD = totalTopic.filter(item => item.lesson.toString() === '634e79fb943636043eb1ba59').length
    let PT = totalTopic.filter(item => item.lesson.toString() === '634e7a69943636043eb1ba6c').length
   
   if (TA > 20){

    return {status : 500, message : "Tiếng Anh đã đủ số câu hỏi"}

   }
   else if (VH > 20){

    return {status : 500, message : "Văn học trong nước đã đủ số câu hỏi"}

   }

   else if (TH > 10){

    return {status : 500, message : "Toán học đã đủ số câu hỏi"}

   }

   else if (TD > 10){

    return {status : 500, message : "Tư duy logic đã đủ số câu hỏi"}

   }

   else if (PT > 10){

    return {status : 500, message : "Phân tích số liệu đã đủ số câu hỏi"}

   }

   else{
    const question = await create({choices, answer,examId, ...rest});

    result = await Exams.findOneAndUpdate(
        { _id: examId, isDeleted: false },
        {
            $push: { questions: { _id: question._id } },
        }
    )

    return {status : 200, message : "Một câu hỏi mới được tạo"}
   }
 
}

module.exports = {
    getAll,
    create,
    getById,
    update,
    deleteQuestion,
    addQuestionToExam,
}
